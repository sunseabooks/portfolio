import { useState } from 'react'
import {Route, Routes} from 'react-router-dom'
import Layout from './components/layout/Layout'
import Test from './Test'
import Book from './components/book/Book'
import Home from './components/home/Home'
import './App.css'

export default function App() {
  const [count, setCount] = useState(0)

  return (
    <>
    <Routes>
      <Route path="/" element={<Layout />} >
        <Route index element={<Home />} />
        <Route path="/test" element={<Test />} />
        <Route path="/resume" element={<Book />} />
      </Route>
    </Routes>
    </>
  )
}
