import React from 'react'
import {Gallery} from '../../types/gallery';

interface Props {
  gallery: Gallery[]
}
const Carousel: React.FC<Props>=({gallery}) => {
  return (
    <div className="carousel">
      <button className="carousel-button prev"></button>
      <button className="carousel-button next"></button>
      <ul>

      </ul>


    </div>
  )
}

export default Carousel
