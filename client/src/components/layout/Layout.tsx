import React from 'react'
import Sidebar from '../sidebar/Sidebar'
import {Outlet} from 'react-router-dom'
import './Layout.css'
export default function Layout() {
  return (
    <>
    <div className="page-layout">
      <Sidebar />
      <div className="sidebar">
      </div>
      <main className="main">
        <Outlet />
      </main>
    </div>
    </>
  )
}
