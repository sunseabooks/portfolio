import React from 'react'
import { NavLink, Link } from 'react-router-dom'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faEnvelope, faHome, faUser} from '@fortawesome/free-solid-svg-icons'
import myLogo from '../../assets/logo.svg'
import './Sidebar.css'
export default function Sidebar() {
  return (
    <div className="sidebar-layout">
        <Link className="logo" to="/">
            <img src={myLogo}className="logo" alt="logo" />
        </Link>
        <nav className="nav-layout">
            <NavLink to="" end>
                <FontAwesomeIcon icon={faHome} size="3x" color="#4d4d4e"/>
            </NavLink>
            <NavLink to="/resume" end>
                <FontAwesomeIcon icon={faUser} size="3x" color="#4d4d4e"/>
            </NavLink>
            <NavLink to="/test" end>
                <FontAwesomeIcon icon={faEnvelope} size="3x" color="#4d4d4e"/>
            </NavLink>
        </nav>
    </div >

  )
}
