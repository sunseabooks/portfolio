import React,{useRef} from 'react'
import './ContactForm.css'
import {env} from "node:process"
import emailjs from '@emailjs/browser'


export default function ContactForm() {
    const form = useRef<HTMLFormElement>(null)

    function sendEmail(e: React.FormEvent) {
        e.preventDefault()
        emailjs.sendForm(
            'gmail',
            'template_tld6omd',
            form.current??"",
            `${env.EMAILJS_PUBLIC_KEY}`
        ).then(result => {
            console.log(result.text)
        }, error => {
            console.log(error.text)
        }).catch( error => {
            console.log(error?.text)
        })

    }
  return (
    <>
    <form ref={form} onSubmit={sendEmail}>
        <input aria-label="contact name" required
            type="text"
            name="contact_name"
            placeholder="Your name"
        />
        <input aria-label="subject" required
            type="text"
            name="subject"
            placeholder="Subject"
        />
        <input aria-label="contact email" required
            type="email"
            name="contact_email"
            placeholder="Your Email"
        />
        <input aria-label="message" required
            type="text"
            name="message"
            placeholder="Message..."
        />
    </form>
    </>
  )
}
