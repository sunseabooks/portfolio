import React from 'react'
import resume from '../../assets/Resume.svg'
import './PageLeft.css'
export default function PageLeft() {
  return (
    <div className="pageleft-layout">
        <img src={resume} alt="alt" />
    </div>
  )
}
