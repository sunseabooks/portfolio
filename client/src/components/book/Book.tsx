import React from 'react'
import PageLeft from '../pageLeft/PageLeft'
import PageRight from '../pageRight/PageRight'
import './Book.css'
export default function Book() {
  return (
    <div className="book-layout">
      <PageLeft />
      <PageRight />
    </div>
  )
}
