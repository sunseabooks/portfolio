import express, { Request, Response } from "express"
import TestModel from '../models/test'

const router = express.Router()
router.use(express.json())

router.get('', (req, res) => {
    res.status(200).send('testing route')
})

router.post('', async (req: Request, res:Response) => {

    const test = await TestModel.create({title:req.body.title, body:req.body.body})
    res.status(200).json(test)
})

export default router
