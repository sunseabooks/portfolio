import mongoose from 'mongoose'

const TestSchema = new mongoose.Schema({
    title: String,
    body: String
})

const TestModel = mongoose.model('Test', TestSchema)

export default TestModel
