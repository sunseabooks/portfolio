import {env} from 'node:process'
import express, { Request, Response, Router } from 'express'
import mongoose from "mongoose"
import testRouter from './routes/test'


const app = express()
const port = 8000

app.get('/', (req:Request, res:Response) => {
    res.json({"message": "aaaaaaaaaa"})
})
app.use('/test', testRouter)

const db_url:string = env.DATABASE_URL ?? ""

mongoose.connect(
    db_url
).then( () =>{
    app.listen(port, () => {
        console.log(`example app listen ${port}`)
    })
})
